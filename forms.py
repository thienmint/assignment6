# Flask-WTF forms
from flask_wtf import FlaskForm
from wtforms import (StringField, TextAreaField, DecimalField,
                     PasswordField, SubmitField, IntegerField)
from wtforms.validators import (DataRequired, ValidationError,
                                InputRequired, NumberRange, Optional)


class MovieForm(FlaskForm):

    year = IntegerField("Year", validators=[
        NumberRange(min=1878, max=2100), Optional()
    ])
    delete_tile = title = StringField("Title", validators=[Optional()])
    director = StringField("Director", validators=[Optional()])
    search_actor = actor = StringField("Actor", validators=[Optional()])
    release_date = StringField("Release Date", validators=[Optional()])
    rating = DecimalField("Rating", validators=[
        NumberRange(min=0, max=10), Optional()
    ])

