from pony.orm import *
from decimal import Decimal
import os
db = Database()


class Movie(db.Entity):
    id = PrimaryKey(int, auto=True)
    year = Optional(int)
    title = Optional(str, unique=True)
    director = Optional(str)
    actor = Optional(str)
    release_date = Optional(str)
    rating = Optional(Decimal)


# db.bind(provider='sqlite', filename='users.db', create_db=True)

database = os.environ.get("RDS_DB_NAME", None)
username = os.environ.get("RDS_USERNAME", None)
password = os.environ.get("RDS_PASSWORD", None)
hostname = os.environ.get("RDS_HOSTNAME", None)

db.bind(provider='mysql',
        user=username,
        passwd=password,
        host=hostname,
        db=database)

db.generate_mapping(create_tables=True)
