# System stuff
from __future__ import with_statement, print_function
import traceback
from pprint import pprint
# MySQL Stuff
from models import *
# Flask Stuff
from flask import Flask, render_template, jsonify, abort, request, Response
from forms import MovieForm


# Setting up Flask App
app = application = Flask(__name__)
app.config['WTF_CSRF_ENABLED'] = False


@app.route('/')
def home():

    return render_template('index.html')


@app.route('/add_movie', methods=['POST'])
def add_movie():
    resp = jsonify("No response from server")
    resp.status_code = 500
    try:
        inputs = request.form
        form = MovieForm(inputs)
        if form.validate():
            add_input = dict()
            for key in inputs:
                add_input[key] = inputs[key]
            if len(add_input['rating'].strip()) > 0:
                add_input['rating'] = Decimal(add_input['rating'])
            else:
                add_input['rating'] = None

            if len(add_input['year'].strip()) == 0:
                add_input['year'] = None

            add_input['title'] = add_input['title'].title()
            add_input['actor'] = add_input['actor'].title()

            try:
                with db_session:
                    m = Movie(**add_input)
            except Exception as e:
                resp = jsonify('Movie "{0}" could not be inserted\n{1}'
                               .format(add_input['title'], e)
                               .replace('\n', '<br/>')
                               )
                resp.status_code = 500
            else:
                resp = jsonify('Movie "{0}" successfully inserted'
                               .format(m.title))
                resp.status_code = 200
        else:
            print(form.errors)
            resp = jsonify(form.errors)
            resp.status_code = 400
    except Exception as e:
        abort(500, traceback.format_exc().replace('\n', '<br/>'))
    else:
        return resp


@app.route('/update_movie', methods=['POST'])
def update_movie():
    resp = jsonify("No response from server")
    resp.status_code = 500
    try:
        inputs = request.form
        form = MovieForm(inputs)
        if form.validate():
            update_input = dict()
            for key in inputs:
                update_input[key] = inputs[key]
            if len(update_input['rating'].strip()) > 0:
                update_input['rating'] = Decimal(update_input['rating'])
            else:
                update_input['rating'] = None

            if len(update_input['year'].strip()) == 0:
                update_input['year'] = None

            update_input['title'] = update_input['title'].title()
            
            try:
                with db_session:
                    m = Movie.get(title=update_input['title'])
                    m.set(**update_input)
            except Exception as e:
                resp = jsonify('Movie "{0}" could not be updated\n{1}'
                               .format(update_input['title'], e)
                               .replace('\n', '<br/>')
                               )
                resp.status_code = 500
                return resp
            else:
                resp = jsonify('Movie "{0}" successfully updated'
                               .format(m.title))
                resp.status_code = 200
                return resp
        else:
            print(form.errors)
            resp = jsonify(form.errors)
            resp.status_code = 400
            return resp
    except Exception as e:
        abort(500, traceback.format_exc().replace('\n', '<br/>'))


@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    resp = jsonify("No response from server")
    resp.status_code = 500
    try:
        inputs = request.form
        form = MovieForm(inputs)

        if form.validate():
            title = inputs['delete_title'].title()
            try:
                with db_session:
                    if Movie.exists(title=title):
                        m = Movie.get(title=title)
                        m.delete()
                    else:
                        resp = jsonify('Movie "{0}" does not exist'
                                       .format(inputs['delete_title']))
                        resp.status_code = 500
                        return resp
            except Exception as e:
                resp = jsonify('Movie "{0}" could not be deleted\n{1}'
                               .format(inputs['delete_title'], e)
                               .replace('\n', '<br/>')
                               )
                resp.status_code = 500
                return resp
            else:
                resp = jsonify('Movie "{0}" successfully deleted'
                               .format(inputs['delete_title']))
                resp.status_code = 200
                return resp
        else:
            print(form.errors)
            resp = jsonify(form.errors)
            resp.status_code = 400
            return resp
    except Exception as e:
        resp = jsonify(traceback.format_exc().replace('\n', '<br/>'))
        resp.status_code = 500
        return resp


@app.route('/search_movie', methods=['POST'])
def search_movie():
    resp = jsonify("No response from server")
    resp.status_code = 500
    try:
        inputs = request.form
        form = MovieForm(inputs)

        if form.validate():
            actor = inputs['search_actor'].title()
            with db_session:
                movies = Movie.select(lambda m: m.actor == actor)
                output = []
                for m in movies:
                    output.append('|title: "{0}", year: {1}, actor: "{2}"|'.format(
                        m.title,
                        m.year,
                        m.actor
                    ))

                if len(output) == 0:
                    resp = jsonify('No movies found for actor "{0}"'
                                   .format(inputs['search_actor']))
                    resp.status_code = 500
                    return resp
                else:
                    resp = jsonify(u'<br/>'.join(output))
                    resp.status_code = 200
                    return resp
        else:
            print(form.errors)
            resp = jsonify(form.errors)
            resp.status_code = 400
            return resp
    except Exception as e:
        resp = jsonify(traceback.format_exc().replace('\n', '<br/>'))
        resp.status_code = 500
        return resp


@app.route('/highest_rating', methods=['POST'])
def highest_rating():
    resp = jsonify("No response from server")
    resp.status_code = 500
    try:
        with db_session:
            highest = max(m.rating for m in Movie)
            movies = Movie.select(lambda m: m.rating == highest)
            output = []
            for m in movies:
                output.append(
                    ('|title: "{0}", year: {1}, actor: "{2}", director: "{3}", rating: "{4}"|'
                     .format(m.title, m.year, m.actor, m.director, m.rating)))
            if len(output) == 0:
                output.append("No movie in database. Try to add one!")

            resp = jsonify('<br/>'.join(output))
            resp.status_code = 200
            return resp
    except Exception as e:
        resp = jsonify(traceback.format_exc().replace('\n', '<br/>'))
        resp.status_code = 500
        return resp


@app.route('/lowest_rating', methods=['POST'])
def lowest_rating():
    resp = jsonify("No response from server")
    resp.status_code = 500
    try:
        with db_session:
            lowest = min(m.rating for m in Movie)
            movies = Movie.select(lambda m: m.rating == lowest)
            output = []
            for m in movies:
                output.append(
                    ('|title: "{0}", year: {1}, actor: "{2}", director: "{3}", rating: "{4}"|'
                     .format(m.title, m.year, m.actor, m.director, m.rating)))
            if len(output) == 0:
                output.append("No movie in database. Try to add one!")

            resp = jsonify('<br/>'.join(output))
            resp.status_code = 200
            return resp
    except Exception as e:
        resp = jsonify(traceback.format_exc().replace('\n', '<br/>'))
        resp.status_code = 500
        return resp


# run the app.
if __name__ == "__main__":
    # Setting debug to True enables debug output. This line should be
    # removed before deploying a production app.
    application.run('0.0.0.0')